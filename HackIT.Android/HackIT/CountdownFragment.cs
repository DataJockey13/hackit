using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;

namespace HackIT
{
    public class CountdownFragment : DialogFragment
    {        
        TextView countdown;
        Handler tickHandler;
        Action tickAction;
        int count;
        bool paused = false;

        public delegate void CountdownDone();
        public event CountdownDone OnCountdownDone;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.Countdown, container, false);
            countdown = view.FindViewById<TextView>(Resource.Id.countdown);
            countdown.Click += delegate {
                OnCountdownDone?.Invoke();
                Dismiss();
            };

            if (savedInstanceState != null)
            {
                count = savedInstanceState.GetInt("count");
            }
            else
                count = 4;

            tickHandler = new Handler();
            tickAction = Tick;

            //view.FindViewById<LinearLayout>(Resource.Id.dialogLayout).SetBackgroundColor(Android.Graphics.Color.Transparent);
            return view;
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutInt("count", count);
        }

        public override void OnResume()
        {
            base.OnResume();
            paused = false;
            tickHandler.PostDelayed(tickAction, 1000);
        }

        public override void OnPause()
        {
            tickHandler.RemoveCallbacks(tickAction); //stop
            paused = true;
            base.OnPause();
        }

        private void Tick()
        {
            if (!paused)
            {
                count--;
                if (count > 0)
                {
                    countdown.Text = count.ToString();

                    //countdown.Animation 
                    var scaleUp = AnimationUtils.LoadAnimation(Context, Resource.Animation.scaleUp);
                    countdown.StartAnimation(scaleUp);
                    tickHandler.PostDelayed(tickAction, 1000);
                }
                else
                {
                    tickHandler.RemoveCallbacks(tickAction); //stop
                    OnCountdownDone?.Invoke();
                    Dismiss();
                }
            }
        }
    }
}