using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HackIT.Shared;

namespace HackIT
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Button btNewGame;
        Button btResumeGame;
        Button btDebug9;
        Button btDebug10;
        Button btHighscore;
        Button btHelp;
        Button btStatistics;
        bool SaveGameExists = false;

        private void StartNewGame(int debugLv)
        {
            if (SaveGameExists)
            {
                var callDialog = new AlertDialog.Builder(this);
                callDialog.SetMessage("This will crash and override your savegame! Proceed?");
                callDialog.SetNeutralButton("doit", delegate
                {
                    SaveGameExists = false;
                    StartNewGame(debugLv);
                });
                callDialog.SetNegativeButton("nee", delegate { });

                // Show the alert dialog to the user and wait for response.
                callDialog.Show();
            }
            else
            {
                var intent = new Intent(this, typeof(GameActivity));
                if (debugLv > 0)
                    intent.PutExtra("action", debugLv);
                StartActivity(intent);

            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            btNewGame = FindViewById<Button>(Resource.Id.btNewGame);
            btResumeGame = FindViewById<Button>(Resource.Id.btResumeGame);
            btDebug9 = FindViewById<Button>(Resource.Id.btDebug9);
            btDebug10 = FindViewById<Button>(Resource.Id.btDebug10);
            btHighscore = FindViewById<Button>(Resource.Id.btHighscore);
            btHelp = FindViewById<Button>(Resource.Id.btHelp);
            btStatistics = FindViewById<Button>(Resource.Id.btStatistics);


            btResumeGame.Enabled = false;

            btDebug9.Click += (sender, e) =>
            {
                StartNewGame(9);
            };
            btDebug10.Click += (sender, e) =>
            {
                StartNewGame(10);
            };

            btNewGame.Click += (sender, e) =>
            {
                StartNewGame(0);
            };

            btResumeGame.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(GameActivity));
                intent.PutExtra("savegame", true);
                StartActivity(intent);
            };
            btStatistics.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(StatisticActivity));
                StartActivity(intent);
            };
            btHelp.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(HelpActivity));
                StartActivity(intent);
            };
            btHighscore.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(SurpriseActivity));
                StartActivity(intent);
            };
        }

        protected override void OnStart()
        {
            base.OnStart();

            ISharedPreferences prefs = GetSharedPreferences("savegame", FileCreationMode.Private);
            SaveGameExists = (prefs != null) && prefs.GetBoolean("savestate", false);
            btResumeGame.Enabled = SaveGameExists;
        }

 
    }
}