﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content.Res;
using Android.Content;
using Android.Graphics;
using Android.Content.PM;
using System.Collections.Generic;
using HackIT.Shared;

namespace HackIT
{
    [Activity(Label = "@string/ApplicationName", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class GameActivity : Activity
    {
        Button buttonPauseResume;
        Button buttonProxy;
        Button buttonBuyProxy;
        Button buttonBackdoor;
        Button buttonBuyBackdoor;
        Button buttonDDoS;
        Button buttonBuyDDoS;
        Button buttonX;
        Button buttonO;
        Button buttonI;
        TextView textReputation;
        TextView textCash;
        TextView textProgress;
        TextView textCode;
        TextView textLog;
        TextView textHost;
        TextView textCodeMsg;
        List<TextView> textCodes;
        ProgressBar progress;
        Game game;
        ColorStateList defaultCodeColors;
        CountdownFragment Countdown;


        Handler tickHandler;
        Action tickAction;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Game);

            buttonPauseResume = FindViewById<Button>(Resource.Id.buttonPauseResume);
            buttonProxy = FindViewById<Button>(Resource.Id.buttonProxy);
            buttonBuyProxy = FindViewById<Button>(Resource.Id.buttonBuyProxy);
            buttonBackdoor = FindViewById<Button>(Resource.Id.buttonBackdoor);
            buttonBuyBackdoor = FindViewById<Button>(Resource.Id.buttonBuyBackdoor);
            buttonDDoS = FindViewById<Button>(Resource.Id.buttonDDoS);
            buttonBuyDDoS = FindViewById<Button>(Resource.Id.buttonBuyDDoS);
            buttonX = FindViewById<Button>(Resource.Id.buttonX);
            buttonO = FindViewById<Button>(Resource.Id.buttonO);
            buttonI = FindViewById<Button>(Resource.Id.buttonI);
            textReputation = FindViewById<TextView>(Resource.Id.textReputation);
            textCash = FindViewById<TextView>(Resource.Id.textCash);
            textProgress = FindViewById<TextView>(Resource.Id.textProgress);
            textCode = FindViewById<TextView>(Resource.Id.textCode);
            textLog = FindViewById<TextView>(Resource.Id.textLog);
            textProgress = FindViewById<TextView>(Resource.Id.textProgress);
            textHost = FindViewById<TextView>(Resource.Id.textHost);
            progress = FindViewById<ProgressBar>(Resource.Id.progress);
            textCodeMsg = FindViewById<TextView>(Resource.Id.textCodeMsg);

            textCodes = new List<TextView>();
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode1));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode2));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode3));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode4));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode5));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode6));
            textCodes.Add(FindViewById<TextView>(Resource.Id.textCode7));

            defaultCodeColors = textCodes[6].TextColors;

            Countdown = new CountdownFragment();
            Countdown.SetStyle(DialogFragmentStyle.NoFrame, 0);
            Countdown.OnCountdownDone += Countdown_OnCountdowDone;

            if ((bundle != null) && bundle.GetBoolean("savestate"))
            {
                string saveData = bundle.GetString("savedata");
                game = new Game(saveData);
            }
            else 
                if (Intent.GetBooleanExtra("savegame", false) 
                    && (GetSharedPreferences("savegame", FileCreationMode.Private).GetBoolean("savestate", false)))
            {
                string saveData = GetSharedPreferences("savegame", FileCreationMode.Private).GetString("savedata", "");
                game = new Game(saveData);
            }
            else
            {
                game = new Game(Intent.GetIntExtra("action", 0));
            }

            buttonX.Click += delegate { Register("X"); };
            buttonO.Click += delegate { Register("O"); };
            buttonI.Click += delegate { Register("I"); };

            buttonProxy.Click += delegate 
            {
                RefreshComponents();
            };
            buttonBuyProxy.Click += delegate 
            {
                game.AddUtility(Game.Utility.Proxy);
                RefreshComponents();
            };
            buttonBackdoor.Click += delegate 
            {
                game.ActivateUtility(Game.Utility.Backdoor);
                RefreshComponents();
            };
            buttonBuyBackdoor.Click += delegate
            {
                game.AddUtility(Game.Utility.Backdoor);
                RefreshComponents();
            };
            buttonDDoS.Click += delegate
            {
                game.ActivateUtility(Game.Utility.DDoS);
                RefreshComponents();
            };
            buttonBuyDDoS.Click += delegate
            {
                game.AddUtility(Game.Utility.DDoS);
                RefreshComponents();
            };

            tickHandler = new Handler();
            tickAction = Tick;

            buttonPauseResume.Click += (sender, e) =>
            {
                if (!game.Started || game.Paused || game.Finished)
                {
                    ShowCountdown();
                }
                else
                    game.Pause();
            };

            RefreshComponents();
        }

        private void Countdown_OnCountdowDone()
        {
            if (!game.Started || game.Finished)
            {
                game.Start();
            }
            else
                if (game.Paused)
            {
                game.Resume();
            }

            if (game.Started && !game.Paused && !game.Finished)
            {
                tickHandler.PostDelayed(tickAction, 1000 / Game.TicksPerSecond);
            }
        }

        protected override void OnPause()
        {
            base.OnPause();

            ISharedPreferences prefs = GetSharedPreferences("savegame", FileCreationMode.Private);
            ISharedPreferencesEditor prefsEdit = prefs.Edit();

            if (Countdown.IsVisible)
                Countdown.Dismiss();

            if ((game is Game) && game.Active)
            {
                game.Pause();
                tickHandler.RemoveCallbacks(tickAction);
            }

            if ((game is Game) && (game.Started))
            {
                prefsEdit.PutBoolean("savestate", true);
                prefsEdit.PutString("savedata", game.SaveData());
            }
            else
            {
                prefsEdit.PutBoolean("savestate", false);
                prefsEdit.PutString("savedata", "");
            }

            prefsEdit.Apply();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            if ((game is Game) && (game.Started))
            {
                string savedata = game.SaveData();

                outState.PutBoolean("savestate", true);
                outState.PutString("savedata", savedata);
            }
        }

        private void Tick()
        {
            //timer.Change(Timeout.Infinite, 100);

            if (game.Active)
                game.Tick();
            RefreshComponents();

            if (game.Active)
            {
                tickHandler.PostDelayed(tickAction, 1000 / Game.TicksPerSecond); //rewind
            }
            else tickHandler.RemoveCallbacks(tickAction); //stop
        }

        private void Register(string key)
        {
            game.Register(key);
            RefreshComponents();
        }

        private void ShowCountdown()
        {
            FragmentTransaction ft = FragmentManager.BeginTransaction();
            //Remove fragment else it will crash as it is already added to backstack
            Fragment prev = FragmentManager.FindFragmentByTag("countdown");
            if (prev != null)
            {
                ft.Remove(prev);
            }
            ft.AddToBackStack(null);

            //Add fragment
            Countdown.Show(FragmentManager, "countdown");
        }

        private void RefreshComponents()
        {
            buttonI.Enabled = game.Active;
            buttonO.Enabled = game.Active;
            buttonX.Enabled = game.Active;

            buttonProxy.Text = !game.IsUtility(Game.Utility.Proxy) ? "Proxy" : string.Format("Proxy ({0})", game.Proxy.Count);
            buttonBackdoor.Text = !game.IsUtility(Game.Utility.Backdoor) ? "Backdoor" : string.Format("Backdoor ({0})", game.Backdoor.Count);
            buttonDDoS.Text = !game.IsUtility(Game.Utility.DDoS) ? "DDoS" : string.Format("DDoS ({0})", game.DDoS.Count);

            buttonProxy.Enabled = game.IsUtility(Game.Utility.Proxy);
            buttonBackdoor.Enabled = game.IsUtility(Game.Utility.Backdoor);
            buttonDDoS.Enabled = game.IsUtility(Game.Utility.DDoS);

            buttonBuyProxy.Enabled = game.Cash >= game.Proxy.Value;
            buttonBuyBackdoor.Enabled = game.Cash >= game.Backdoor.Value;
            buttonBuyDDoS.Enabled = game.Cash >= game.DDoS.Value;

            buttonBuyProxy.Text = string.Format("{0:c0}", game.Proxy.Value);
            buttonBuyBackdoor.Text = string.Format("{0:c0}", game.Backdoor.Value);
            buttonBuyDDoS.Text = string.Format("{0:c0}", game.DDoS.Value);

            if (!game.Started)
                buttonPauseResume.Text = GetString(Resource.String.Start);
            else
                if (game.Paused)
                buttonPauseResume.Text = GetString(Resource.String.Resume);
            else
                if (game.Finished)
                buttonPauseResume.Text = GetString(Resource.String.Start);
            else
                buttonPauseResume.Text = GetString(Resource.String.Pause);

            progress.Max = game.MaxTicks;
            progress.Progress = game.Ticks;

            int percent = game.Ticks * 100 / game.MaxTicks;
            if (percent <= 33)
                progress.ProgressTintList = ColorStateList.ValueOf(Color.LightGreen);
            else if (percent <= 66)
                progress.ProgressTintList = ColorStateList.ValueOf(Color.Argb(255, 120, 200, 200));
            else if (percent <= 90)
                progress.ProgressTintList = ColorStateList.ValueOf(Color.Yellow);
            else
                progress.ProgressTintList = ColorStateList.ValueOf(Color.Red);


            textHost.Text = string.Format("{0} Host ({1:c0} /s)", game.Host, game.Payment);
            textReputation.Text = game.Reputation;
            textCash.Text = string.Format("{0:c0}", game.Cash);
            textCodeMsg.Text = "";

            for (int i = 0; i < textCodes.Count; i++)
            {
                TextView view = textCodes[i];
                if (i < game.Code.Length)
                {
                    if (i < game.Input.Length)
                    {
                        view.Text = game.Input[i].ToString();
                        if (game.Input[i].Equals(game.Code[i]))
                            view.SetTextColor(Color.Green);
                        else
                        {
                            view.SetTextColor(Color.Red);
                            textCodeMsg.Text = "access denied";
                            textCodeMsg.SetTextColor(Color.Red);
                        }
                    }
                    else if (!game.Active && !game.Finished)
                    {
                        view.Text = "?";
                        view.SetTextColor(defaultCodeColors);
                    }
                    else
                    {
                        view.Text = game.Code[i].ToString();
                        view.SetTextColor(defaultCodeColors);
                    }
                }
                else
                {
                    view.Text = "";
                    view.SetTextColor(defaultCodeColors);
                }
            }

            if (!game.Code.Length.Equals(0) && game.Code.Equals(game.Input))
            {
                textCodeMsg.Text = "access granted";
                textCodeMsg.SetTextColor(Color.Green);
            }

            textLog.Text = game.Log.Text();
            textProgress.Text = string.Format("{0} / {1}", game.Progress, Game.CodesPerLevel);
        }
    }
}

