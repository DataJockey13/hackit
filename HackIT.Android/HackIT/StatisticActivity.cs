using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HackIT.Shared;

namespace HackIT
{
   
 
    [Activity(Label = "Statistics")]
    public class StatisticActivity : ListActivity
    {
        private Random rng;
        private StatisticsAdapter listAdapter;
        //private ArrayAdapter<string> listAdapter;

        private void FillList()
        {
            List<Statistic> stats = DataManager.GetStatistcs();
            /*
            string[] statsarray = new string[stats.count];
            for (int i = 0; i < stats.count; i++)
            {
                statistic stat = stats[i];
                statsarray[i] = string.format("{0} host (min {1}, max {2}, avarage {3})", game.hostname(stat.level), stat.min, stat.max, stat.average);
            }

            listadapter = new arrayadapter<string>(this, android.resource.layout.simplelistitem1, statsarray);
            */


            listAdapter = new StatisticsAdapter(this, stats);
            ListAdapter = listAdapter;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            rng = new Random();

            FillList();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {            
            MenuInflater.Inflate(Resource.Menu.StatisticsMenu, menu);

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.StatisticsMenuWipe: DataManager.WipeStatistics();
                    break;
                case Resource.Id.StatisticsMenu2:
                    DataManager.UpdateStatistic(1 + rng.Next(3) , 7 + rng.Next(20));//1..3
                    break;
                case Resource.Id.StatisticsMenu3:
                    DataManager.UpdateStatistic(4 + rng.Next(3), 12 + rng.Next(20)); //4..6
                    break;
                case Resource.Id.StatisticsMenu4:
                    DataManager.UpdateStatistic(7 + rng.Next(3), 14 + rng.Next(20)); //7..9
                    break;
                case Resource.Id.StatisticsMenu5:
                    DataManager.UpdateStatistic(10 + rng.Next(3), 20 + rng.Next(20)); //10..12
                    break;
                case Resource.Id.StatisticsMenu10:
                    for (int i=0;i < rng.Next(100);i++)
                    {
                        DataManager.UpdateStatistic(1 + rng.Next(13), 5 + rng.Next(40)); 
                    }

                    break;
            }

            FillList();
            //listAdapter.NotifyDataSetChanged();

            return base.OnOptionsItemSelected(item);
        }
    }
}