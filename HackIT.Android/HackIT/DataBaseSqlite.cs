using System;
using System.Collections.Generic;
using Android.Content;
using Android.Database.Sqlite;
using Android.Database;
using HackIT.Shared;

namespace HackIT
{
    /// <summary>
    /// Versuch einer Sqlite Schnittstelle mit SQLiteOpenHelper
    /// ...funktioniert leider nur unter Android wegen DataBaseSqlite(Context context)
    /// </summary>
    class DataBaseSqlite : SQLiteOpenHelper
    {
        public static readonly string create_table_sql =
           "CREATE TABLE [Statistics] ([lv] INTEGER PRIMARY KEY NOT NULL UNIQUE ASC, [Count] INTEGER, [Sum] INTEGER, [Min] INTEGER, [Max] INTEGER)";

        public static readonly int DatabaseVersion = 0;
        public static readonly string Database = "Database.db3";

        public DataBaseSqlite(Context context) : base(context, Database, null, DatabaseVersion) { }

        public override void OnCreate(SQLiteDatabase db)
        {
            db.ExecSQL(create_table_sql);
        }

        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            throw new NotImplementedException();
        }

        /// <summary>Convert from DataReader to Task object</summary>
        private Statistic StatisticFromCursor(ICursor cursor)
        {
            var stat = new Statistic();
            stat.Level = cursor.GetInt(0);
            stat.Count = cursor.GetInt(1);
            stat.Sum = cursor.GetInt(2);
            stat.Min = cursor.GetInt(3);
            stat.Max = cursor.GetInt(4);
            return stat;
        }

        public IEnumerable<Statistic> GetStatistics()
        {
            var tl = new List<Statistic>();
            ICursor cursor =  ReadableDatabase.RawQuery("SELECT lv, Count, Sum, Min, Max FROM Statistics", null);
            while (cursor.MoveToNext())
            {
                tl.Add(StatisticFromCursor(cursor));
            }
            cursor.Close();
            return tl;
        }

        public Statistic GetStatistic(int Level)
        {
            Statistic t;
            ICursor cursor = ReadableDatabase.RawQuery("SELECT lv, Count, Sum, Min, Max FROM Statistics", null);// "WHERE lv=" + Level);
            if (cursor.MoveToFirst())
            {
                t = StatisticFromCursor(cursor);
            } else {
                t = new Statistic();
                t.Level = Level;
            }
            cursor.Close();

            return t;
        }

        public void SaveStatistic(Statistic item)
        {
            WritableDatabase.ExecSQL(string.Format("UPDATE Statistics SET Count = {0}, Sum = {1}, Min = {2}, Max = {3} WHERE lv = {4}", item.Count, item.Sum, item.Min, item.Max, item.Level));
        }

        public void DeleteStatistic(int Level)
        {
            WritableDatabase.ExecSQL(string.Format("DELETE FROM Statistics WHERE lv = {0}", Level));
        }

        public void WipeStatistics()
        {
            WritableDatabase.ExecSQL("DELETE FROM Statistics");
        }
    }
}