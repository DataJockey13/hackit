using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using HackIT.Shared;

namespace HackIT
{
    class StatisticsAdapter : BaseAdapter<Statistic>
    {
        Activity context = null;
        List<Statistic> list;

        public StatisticsAdapter(Activity context, List<Statistic> list): base()
        {
            this.context = context;
            this.list = list;
        }

        public override Statistic this[int position]
        {
            get
            {
                return list[position];
            }
        }

        public override int Count
        {
            get
            {
                return list.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return list[position].Level;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = list[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.StatisticsRow, null);

            view.FindViewById<TextView>(Resource.Id.textStatsHost).Text = Game.HostName(item.Level) + " host";
            view.FindViewById<TextView>(Resource.Id.textStatsMin).Text = string.Format("{0} min", item.Min);
            view.FindViewById<TextView>(Resource.Id.textStatsMax).Text = string.Format("{0} max", item.Max);
            view.FindViewById<TextView>(Resource.Id.textStatsAvarage).Text = string.Format("{0} avg", item.Average);
            return view;
        }
    }
}