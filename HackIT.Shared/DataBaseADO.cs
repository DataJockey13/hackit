using System;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.IO;
using System.Data;

namespace HackIT.Shared
{
    class DataBaseADO
    {

        static object locker = new object();

        public SqliteConnection connection;

        public string path;

        /// <summary>
        /// Initializes a new instance of the Database
        /// if the database doesn't exist, it will create the database and all the tables.
        /// </summary>
        public DataBaseADO(string dbPath)
        {
            var output = "";
            path = dbPath;
            // create the tables
            bool exists = File.Exists(dbPath);

            if (!exists)
            {
                connection = new SqliteConnection("Data Source=" + dbPath);

                connection.Open();
                var commands = new[] {
                    "CREATE TABLE [Statistics] (_id INTEGER PRIMARY KEY ASC, Count INTEGER, Sum INTEGER, Min INTEGER, Max INTEGER);"
                };
                foreach (var command in commands)
                {
                    using (var c = connection.CreateCommand())
                    {
                        c.CommandText = command;
                        var i = c.ExecuteNonQuery();
                    }
                }
                connection.Close();
            }
            else
            {
                // already exists, do nothing. 
            }
            Console.WriteLine(output);
        }

        /// <summary>Convert from DataReader to Task object</summary>
        private Statistic StatisticFromReader(SqliteDataReader r)
        {
            var stat = new Statistic();
            stat.Level = Convert.ToInt32(r["_id"]);
            stat.Count = Convert.ToInt32(r["Count"]);
            stat.Sum = Convert.ToInt32(r["Sum"]);
            stat.Min = Convert.ToInt32(r["Min"]);
            stat.Max = Convert.ToInt32(r["Max"]);
            stat.Loaded = true;
            return stat;
        }

        public IEnumerable<Statistic> GetStatistics()
        {
            var tl = new List<Statistic>();

            lock (locker)
            {
                connection = new SqliteConnection("Data Source=" + path);
                connection.Open();
                using (var contents = connection.CreateCommand())
                {
                    contents.CommandText = "SELECT [_id], [Count], [Sum], [Min], [Max] from [Statistics]";
                    var r = contents.ExecuteReader();
                    while (r.Read())
                    {
                        tl.Add(StatisticFromReader(r));
                    }
                }
                connection.Close();
            }
            return tl;
        }

        public Statistic GetStatistic(int Level)
        {
            var t = new Statistic();
            t.Level = Level;
            lock (locker)
            {
                connection = new SqliteConnection("Data Source=" + path);
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [_id], [Count], [Sum], [Min], [Max] from [Statistics] WHERE [_id] = ?";
                    command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = Level });
                    var r = command.ExecuteReader();
                    while (r.Read())
                    {
                        t = StatisticFromReader(r);
                        break;
                    }
                }
                connection.Close();
            }
            return t;
        }

        public int SaveStatistic(Statistic item)
        {
            int r;
            lock (locker)
            {
                if (item.Loaded)
                {
                    connection = new SqliteConnection("Data Source=" + path);
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "UPDATE [Statistics] SET [Count] = ?, [Sum] = ?, [Min] = ?, [Max] = ? WHERE [_id] = ?;";
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Count });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Sum });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Min });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Max });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Level });
                        r = command.ExecuteNonQuery();
                    }
                    connection.Close();
                    return r;
                }
                else
                {
                    connection = new SqliteConnection("Data Source=" + path);
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO [Statistics] ([_id], [Count], [Sum], [Min], [Max]) VALUES (?, ?, ? ,?, ?)";
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Level });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Count });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Sum });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Min });
                        command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = item.Max });
                        r = command.ExecuteNonQuery();
                    }
                    connection.Close();
                    return r;
                }

            }
        }

        public int DeleteStatistic(int Level)
        {
            lock (locker)
            {
                int r;
                connection = new SqliteConnection("Data Source=" + path);
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM [Statistics] WHERE [_id] = ?;";
                    command.Parameters.Add(new SqliteParameter(DbType.Int32) { Value = Level });
                    r = command.ExecuteNonQuery();
                }
                connection.Close();
                return r;
            }
        }

        public void WipeStatistics()
        {
            lock (locker)
            {
                connection = new SqliteConnection("Data Source=" + path);
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM [Statistics];";
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}