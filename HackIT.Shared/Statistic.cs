using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.IO;
using SQLite;


namespace HackIT.Shared
{
    [Table("Statistics")]
    class Statistic
    {
        public Statistic()
        {
            Init();
            Loaded = false;
            this.Level = Level;
        }
        public Statistic(int Level)
        {
            Init();
            Loaded = false;
            this.Level = Level;
        }

        public void Init()
        {
            Count = 0;
            Sum = 0;
            Min = 0;
            Max = 0;
        }

        [PrimaryKey, Unique, Column("_id")]
        public int Level { get; set; }
        public int Count { get; set; }
        public int Sum { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }

        [Ignore]
        public bool Loaded { get; set; }

        [Ignore]
        public int Average => Sum / Count;

        public void Update(int Time)
        {
            Count++;
            Sum += Time;
            if (Min == 0)
                Min = Time;
            else
                Min = Math.Min(Min, Time);
            Max = Math.Max(Max, Time);
        }

        public override string ToString()
        {
            return string.Format("[Statistic: Level={0}, Count={1}, Sum={2}, int Min={3}, Max={4}]", Level, Count, Sum, Min, Max);
        }

    }
}