using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HackIT.Shared
{
    class DataManager
    {
        static DataManager()
        {
        }

        public static void UpdateStatistic(int Level, int Time)
        {
            Statistic stat = GetStatistic(Level);
            stat.Update(Time);
            SaveStatistic(stat);
        }

        public static Statistic GetStatistic(int Level)
        {
            return DataRepositoryNET.GetStatistic(Level);
        }

        public static List<Statistic> GetStatistcs()
        {
            return new List<Statistic>(DataRepositoryNET.GetStatistics());
        }

        public static int SaveStatistic(Statistic item)
        {
            return DataRepositoryNET.SaveStatistic(item);
        }

        public static int DeleteStatistic(int id)
        {
            return DataRepositoryNET.DeleteStatistic(id);
        }

        public static void WipeStatistics()
        {
            DataRepositoryNET.WipeStatistics();
        }
    }
}