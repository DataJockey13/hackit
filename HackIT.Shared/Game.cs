using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HackIT.Shared
{
    class Game
    {
        /// <summary>
        /// Utility Arten
        /// </summary>
        public enum Utility { Proxy, Backdoor, DDoS };
        /// <summary>
        /// Laufzeitwerte einer Utility
        /// </summary>
        public class UtilityItem
        {
            public int Count { get; set; }
            public int Value { get; set; }
            public int Used { get; set; }
            public Utility Typ { get; }
            public UtilityItem(Utility typ, int value)
            {
                Typ = typ;
                Count = 0;
                Used = 0;
                Value = value;
            }
        }

        private class Hold
        {
            public delegate void HoldDone();
            public HoldDone OnDone { get; set; }
            public int Ticks { get; set; }
            public Hold(int Ticks, HoldDone OnDone)
            {
                this.OnDone = OnDone;
                this.Ticks = Ticks;
            }
        }
        private Hold hold = null;

        /// <summary>
        /// Status des Spiels
        /// ...zum Speichern und Wiederherstellen
        /// </summary>
        private class State
        {
            /// <summary>
            /// Level komplett, Anzeige Ergebnis
            /// </summary>
            public bool Halted { get; set; }
            /// <summary>
            /// Spiel gestartet?
            /// </summary>
            public bool Started { get; set; }
            /// <summary>
            /// Spiel angehalten?
            /// </summary>
            public bool Paused { get; set; }
            /// <summary>
            /// Spiel vorbei?
            /// </summary>
            public bool Finished { get; set; }
            /// <summary>
            /// aktuelle gesammelte Erfahrung
            /// </summary>
            public int XP { get; set; }
            /// <summary>
            /// aktueller Tick-Wert einer DDoS Utility
            /// </summary>
            public int DDosTicks { get;  set; }
            /// <summary>
            /// Das insgesamt verdiente Geld
            /// </summary>
            public int Income { get;  set; }
            /// <summary>
            /// das aktuell verf�gbare Geld
            /// </summary>
            public int Cash { get; set; }
            /// <summary>
            /// aktueller Code
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// aktuelle Eingabe
            /// </summary>
            public string Input { get; set; }
            /// <summary>
            /// DebugLevel > 0 = DebugMode
            /// </summary>
            public int DebugLevel { get; set; }
            /// <summary>
            /// der n.te Code f�r das Level
            /// </summary>
            public int Progress { get;  set; }
            /// <summary>
            /// Reputation-Level
            /// </summary>
            public int Level { get;  set; }
            /// <summary>
            /// Anzahl der in der Runde vergangenen Ticks
            /// </summary>
            public int Ticks { get; set; }

            public UtilityItem Proxy { get; set; }
            public UtilityItem Backdoor { get; set; }
            public UtilityItem DDoS { get; set; }

            public void Init()
            {
                Started = false;
                Paused = false;
                Finished = false;
                Halted = false;

                XP = 0;
                DDosTicks = 0;
                Income = 0;
                Cash = 0;
                Code = "";
                Input = "";
                DebugLevel = 0;
                Progress = 1;
                Level = 1;
                Ticks = 0;

                Proxy = new UtilityItem(Utility.Proxy, 10000);
                Backdoor = new UtilityItem(Utility.Backdoor, 5000);
                DDoS = new UtilityItem(Utility.DDoS, 50000);
            }

            public void StartGame()
            {
                Started = true;
                Paused = false;
                Finished = false;
                Input = "";
                Level = 1;
                XP = 0;
                DDosTicks = 0;
                Income = 0;
                Cash = 0;

                if (DebugLevel > 0)
                {
                    Level = DebugLevel;
                    XP = Level * 1000;
                    Cash = Level * 10000;
                }

                StartLevel();
            }
            public void StartLevel()
            {
                Progress = 1;
                Ticks = 0;
                DDosTicks = 0;
                Proxy.Used = 0;
                Backdoor.Used = 0;
                DDoS.Used = 0;
            }
        }

        /// <summary>
        /// Ticks / Sekunde
        /// </summary>
        static public int TicksPerSecond = 10;
        /// <summary>
        /// Wieviel Ticks l�uft eine DDoS Utility
        /// </summary>
        static public int TicksPerDDoS = 200;
        /// <summary>
        /// Wieviel Codes m�ssen in dem aktuellen Level eingegeben werden (n)
        /// </summary>
        static public int CodesPerLevel = 10;

        private Random rng;
        private int DigitsPerCode
        {
            get
            {
                switch (Level)
                {
                    case 1: return 3;
                    case 2: return 4;
                    case 3: return 5;
                    case 4: return 5;
                    case 5: return 6;
                    case 6: return 7;
                    case 7: return 7;
                    case 8: return 7;
                    case 9: return 7;
                    case 10: return 7; //10+ = UV+ Legend 
                    default: return 7; //10+ = UV+ Legend
                }
            }
        }
        private int Digits
        {
            get
            {
                switch (Level)
                {
                    case 1: return 2;
                    case 2: return 2;
                    case 3: return 2;
                    case 4: return 3;
                    case 5: return 3;
                    case 6: return 3;
                    case 7: return 3;
                    case 8: return 3;
                    case 9: return 3;
                    case 10: return 3;
                    default: return 3;
                }
            }
        }
        private State state;
        private void StartLevel()
        {
            state.StartLevel();
            hold = null;            
        }

        public UtilityItem Proxy => state.Proxy;
        public UtilityItem Backdoor => state.Backdoor;
        public UtilityItem DDoS => state.DDoS;

        /// <summary>
        /// Textbeschreibung des Spielverlaufs
        /// </summary>
        public List<string> Log { get; private set; }

        /// <summary>
        /// Spiel gestartet?
        /// </summary>
        public bool Started => state.Started;

        /// <summary>
        /// Spiel angehalten?
        /// </summary>
        public bool Paused => state.Paused;

        /// <summary>
        /// Ticks werden verarbeitet
        /// </summary>
        public bool Active => Started && !Paused && !Finished;

        /// <summary>
        /// Spiel vorbei?
        /// </summary>
        public bool Finished => state.Finished;

        public bool Halted => state.Halted;

        /// <summary>
        /// Das insgesamt verdiente Geld
        /// </summary>
        public int Income => state.Income;

        /// <summary>
        /// das aktuell verf�gbare Geld
        /// </summary>
        public int Cash => state.Cash;

        /// <summary>
        /// Reputation-Level
        /// </summary>
        public int Level => state.Level;

        /// <summary>
        /// gesammelte Erfahrung
        /// </summary>
        public int XP => state.XP;

        /// <summary>
        /// Beschreibung des Levels
        /// </summary>
        public string Reputation
        {
            get
            {
                switch (Level)
                {
                    case 1: return "Noname";
                    case 2: return "Newbie";
                    case 3: return "Script kiddie";
                    case 4: return "Casual";
                    case 5: return "Hacker";
                    case 6: return "Profi";
                    case 7: return "Geek";
                    case 8: return "Guru";
                    case 9: return "Wizzard";
                    case 10: return "Legend";
                    default: return string.Format("Legend ({0})", Level -10);
                }

            }
        }

        /// <summary>
        /// Beschreibung des Hosts
        /// </summary>
        static public string HostName(int Level)
        {
            switch (Level)
            {
                case 1: return "white";
                case 2: return "gray";
                case 3: return "black";
                case 4: return "blue";
                case 5: return "green";
                case 6: return "yellow";
                case 7: return "orange";
                case 8: return "red";
                case 9: return "UV";
                case 10: return "UV+";
                default: return string.Format("UV+{0}", Level - 10);
            }
        }

        /// <summary>
        /// Beschreibung des aktuellen Hosts (Level-abh�ngig)
        /// </summary>
        public string Host
        {
            get
            {
                return HostName(Level);
            }
        }

        /// <summary>
        /// aktueller Code
        /// </summary>
        public string Code => state.Code;

        /// <summary>
        /// Usereingabe
        /// </summary>
        public string Input => state.Input;

        /// <summary>
        /// der n.te Code f�r das Level
        /// </summary>
        public int Progress => state.Progress;

        /// <summary>
        /// aktueller Tick-Wert einer DDoS Utility
        /// </summary>
        public int DDosTicks => state.DDosTicks;

        /// <summary>
        /// Debug Mode
        /// </summary>
        public bool Debug => state.DebugLevel > 0;

        /// <summary>
        /// Anzahl der zur Verf�gung stehenden Ticks pro Runde (z)
        /// </summary>
        public int MaxTicks {
            get {
                if (Level < 10)
                    return 30 * TicksPerSecond;
                else
                    return (30 - (Level - 9)) * TicksPerSecond;
            }
        }

        /// <summary>
        /// Anzahl der in der Runde vergangenen Ticks
        /// </summary>
        public int Ticks => state.Ticks;

        /// <summary>
        /// Cash pro Sekunde f�r das aktuelle Level
        /// </summary>
        public int Payment
        {
            get
            {
                return (DigitsPerCode * Digits + (30 - MaxTicks / TicksPerSecond)) * 1000 / 30;
            }
        }

        private void Init()
        {
            rng = new Random();
            Log = new List<string>();
            state = new State();
            state.Init();
            hold = null;
        }

        public string SaveData()
        {
            return JsonConvert.SerializeObject(state);
        } 

        public Game(string data) : base()
        {
            Init();
            try
            {
                state = JsonConvert.DeserializeObject<State>(data);

            }
            catch (Exception)
            {
                Init();
            }
        }
        public Game() : base()
        {
            Init();
        }
        public Game(int DebugLevel) : base()
        {
            Init();
            state.DebugLevel = DebugLevel;
        }

        private UtilityItem Utilitys(Utility utility)
        {
            switch (utility)
            {
                case Utility.Proxy: return Proxy;
                case Utility.Backdoor: return Backdoor;
                case Utility.DDoS: return DDoS;
                default: return null;
            }
        }
        public void AddUtility(Utility utility)
        {
            int costs = Utilitys(utility).Value;
            if (costs <= Cash)
            {
                state.Cash -= costs;
                Utilitys(utility).Count++;
                Utilitys(utility).Value += Utilitys(utility).Value / 10;
            }
        }
        public bool ActivateUtility(Utility utility)
        {
            if (IsUtility(utility))
            {
                Utilitys(utility).Count--;
                Utilitys(utility).Used++;

                AddLog(utility.ToString() + " used");

                switch (utility)
                {
                    case Utility.Proxy:                       
                        break;
                    case Utility.Backdoor:
                        state.Input = Code;
                        Register("");
                        break;
                    case Utility.DDoS:
                        state.DDosTicks = TicksPerDDoS;
                        break;
                }
                return true;
            }
            else return false;
        }
        public bool IsUtility(Utility utility)
        {
            return Utilitys(utility).Count > 0;
        }

        /// <summary>
        /// Tick hinzuf�gen
        /// </summary>
        public void Tick()
        {
            if ((hold != null) && (hold.Ticks > 0))
            {
                hold.Ticks--;
                if (hold.Ticks <= 0)
                    hold.OnDone();
            }
            else
            {
                if (DDosTicks > 0)
                    state.DDosTicks--;
                else
                    state.Ticks++;

                if (Ticks > MaxTicks)
                {
                    if (ActivateUtility(Utility.Proxy))
                    {
                        state.Ticks -= 10 * TicksPerSecond;
                    }
                    else
                    {
                        state.Finished = true;
                        SetLog(string.Format("you earned {0:c0} as {1}", Income, Reputation));
                        AddLog("game over");                        
                    }
                }
            }
        }

        public void Start()
        {
            Log.Clear();
            state.StartGame();
            StartLevel();
            GenerateCode();
        }

        /// <summary>
        /// Fortsetzen
        /// </summary>
        public void Resume()
        {
            state.Paused = false;
            AddLog("game resumed");
        }

        public void Pause()
        {
            state.Paused = true;
            AddLog("game paused");
        }

        public void Proceed()
        {
            state.Halted = false;
            StartLevel();
            GenerateCode();
            state.Input = "";
            AddLog("game proceeded");
        }

        public void Register(string Key)
        {
            if (Input.Length < Code.Length)
                state.Input += Key;

            if (Input.Equals(Code))
            {
                state.Progress++;                

                if (Progress > CodesPerLevel)
                {
                    int xpGain = 0;
                    int cashGain = 0;
                    if (Proxy.Used <= 0)
                    {
                        xpGain = (MaxTicks - Ticks) * 30 / TicksPerSecond; //Restzeit[s] * 30
                        cashGain = (MaxTicks - Ticks) * Payment / TicksPerSecond; //Restzeit[s] * Payment                       
                    }

                    state.Ticks += TicksPerDDoS * DDoS.Used - DDosTicks;
                    int Seconds = 10 * Proxy.Used + (Ticks / TicksPerSecond);

                    DataManager.UpdateStatistic(Level, Seconds);

                    SetLog(string.Format("Success! You hacked the host in {0} seconds.You earned {1:c0} and {2}xp", Seconds, cashGain, xpGain));
                    state.Income += cashGain;
                    state.Cash += cashGain;
                    state.XP += xpGain;

                    int lv = 1 + XP / 1000;
                    if (lv != Level)
                    {
                        state.Level = lv;
                        if (Level < 11)
                            AddLog(string.Format("you are now known as a {0}", Reputation));
                        else
                            AddLog(string.Format("you fame rises", Reputation));
                    }

                    state.Halted = true;
                }
                else hold = new Hold(TicksPerSecond / 3, delegate {
                                                                      GenerateCode();
                                                                      state.Input = "";
                                                                  });
            }
            else
            {
                if (!Code.Substring(0, Input.Length).Equals(Input))
                {
                    SetLog("access denied");

                    hold = new Hold(TicksPerSecond / 3, delegate { state.Input = ""; });
                }
            }            
        }

        private void AddLog(string msg)
        {
            Log.Insert(0, msg);

            //trim log to 3 lines
            while (Log.Count > 3)
                Log.RemoveAt(3);
        }

        private void SetLog(string arg)
        {
            Log.Clear();
            Log.Add(arg);
        }

        private void UpdateLog(string arg)
        {
            if (Log.Count > 0)
                Log[0] += arg;
        }

        private string GenerateDigit(int range)
        {
            switch (rng.Next(0, range))
            {
                case 0: return "X";
                case 1: return "O";
                case 2: return "I";
                default: return "X";
            }
        }

        private bool CheckAllDigits(string code)
        {
            return code.Contains("X") && code.Contains("O") && code.Contains("I");
        }

        private void GenerateCode()
        {
            string newCode = "";
            int streak = 1;
            int maxStreak = 1;
            string lastDigit = "";
            string nextDigit;

            for (int i = 1; i <= DigitsPerCode; i++)
            {
                nextDigit = GenerateDigit(Digits);
                if (lastDigit.Equals(nextDigit))
                    streak++;
                else
                {
                    maxStreak = Math.Max(maxStreak, streak);
                    streak = 1;
                }
                lastDigit = nextDigit;
                newCode += nextDigit;
            }

            maxStreak = Math.Max(maxStreak, streak);

            if (!newCode.Equals(Code)
                && ((Level < 7) || CheckAllDigits(newCode))
                && ((Level < 8) || (maxStreak <= 3))
                && ((Level < 9) || (maxStreak <= 2))
                && ((Level < 10) || (maxStreak <= 1))
                )
                state.Code = newCode;
            else
                GenerateCode();
        }        
    }
}