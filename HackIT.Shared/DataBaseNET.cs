﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace HackIT.Shared
{
    class DataBaseNET
    {
        private SQLiteConnection db;

        /// <summary>
        /// Initializes a new instance of the Database
        /// if the database doesn't exist, it will create the database and all the tables.
        /// </summary>
        public DataBaseNET(string dbPath)
        {
            db = new SQLiteConnection(dbPath);
            db.CreateTable<Statistic>();
        }

        public IEnumerable<Statistic> GetStatistics()
        {
            var list = db.Table<Statistic>();

            return list;
            //return db.Table<Statistic>();
        }

        public Statistic GetStatistic(int Level)
        {
            // var item =  db.Get<Statistic>(Level);
            return db.Find<Statistic>(Level) ?? new Statistic(Level);
        }

        public int SaveStatistic(Statistic item)
        {
            return db.InsertOrReplace(item);
        }

        public int DeleteStatistic(int Level)
        {
            return db.Delete<Statistic>(Level);
        }

        public void WipeStatistics()
        {
            db.DeleteAll<Statistic>();
        }
    }


}
