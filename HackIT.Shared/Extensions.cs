﻿using System.Text;
using System.Collections.Generic;

namespace HackIT.Shared
{
    static class Extensions
    {
         public static string Text(this List<string> sl)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var value in sl)
            {
                sb.AppendLine(value);
            }
            return sb.ToString().TrimEnd();
        }
    }
}