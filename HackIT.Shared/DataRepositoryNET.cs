﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HackIT.Shared
{
    class DataRepositoryNET
    {
        DataBaseNET db = null;
        protected static string dbLocation;
        protected static DataRepositoryNET me;

        //class constructor
        static DataRepositoryNET()
        {
            me = new DataRepositoryNET();
        }

        protected DataRepositoryNET()
        {
            // set the db location
            dbLocation = DatabaseFilePath;

            // instantiate the database	
            db = new DataBaseNET(dbLocation);
        }

        public static string DatabaseFilePath
        {
            get
            {
                var sqliteFilename = "Database.db3";
#if NETFX_CORE
				                var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);
#else

#if SILVERLIGHT
				                    // Windows Phone expects a local path, not absolute
				                    var path = sqliteFilename;
#else

#if __ANDROID__
                // Just use whatever directory SpecialFolder.Personal returns
                string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
				                        // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				                        // (they don't want non-user-generated data in Documents)
				                        string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				                        string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
#endif
                var path = Path.Combine(libraryPath, sqliteFilename);
#endif

#endif
                return path;
            }
        }

        public static Statistic GetStatistic(int Level)
        {
            return me.db.GetStatistic(Level);
        }

        public static IEnumerable<Statistic> GetStatistics()
        {
            return me.db.GetStatistics();
        }

        public static int SaveStatistic(Statistic item)
        {
            return me.db.SaveStatistic(item);
        }

        public static int DeleteStatistic(int Level)
        {
            return me.db.DeleteStatistic(Level);
        }
        public static void WipeStatistics()
        {
            me.db.WipeStatistics();
        }
    }
}
